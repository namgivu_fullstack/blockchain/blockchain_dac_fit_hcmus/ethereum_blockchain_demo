import { useState } from 'react';

const Nav = () => {
  const [expand, setExpand] = useState(false);
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light custom-nav">
      <div className="container">
        <a className="navbar-brand" href="/">
          DEMO APP
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarScroll"
          aria-controls="navbarScroll"
          aria-expanded={expand}
          onClick={() => setExpand((prev) => !prev)}
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div
          className={`collapse navbar-collapse ${!expand ? '' : 'show'}`}
          id="navbarScroll"
        >
          <ul
            className={`navbar-nav me-auto mb-2 mb-lg-0 navbar-nav-scroll ${
              !expand ? '' : ''
            }`}
            style={{ '--bs-scroll-height': '100px' }}
          >
            <li className="nav-item">
              <a className="nav-link" aria-current="page" href="/sepolia">
                Sepolia Block
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/hash">
                Hash
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/signature">
                Signature
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/mining">
                Mining
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};
export default Nav;
