import { useDispatch } from 'react-redux';
import { setCurrent } from '../redux/pagecontrolReducer';
const Pagination = ({ prop }) => {
  const dispatch = useDispatch();
  return (
    <nav aria-label="Page navigation example ">
      <ul className="pagination gap justify-content-end">
        <li className="page-item">
          <a className="page-link" onClick={() => dispatch(setCurrent(0))}>
            Start
          </a>
        </li>
        <li className="page-item">
          <a
            className="page-link"
            onClick={() => {
              if (prop.current - 1 >= 0) dispatch(setCurrent(prop.current - 1));
            }}
          >
            Prev
          </a>
        </li>
        <li className="page-item disabled">
          <span className="page-link">
            {'Page ' + prop.current + ' of ' + (prop.total - 1)}
          </span>
        </li>

        <li className="page-item">
          <a
            className="page-link"
            onClick={() => {
              if (prop.current + 1 <= prop.total - 1)
                dispatch(setCurrent(prop.current + 1));
            }}
          >
            Next
          </a>
        </li>
        <li className="page-item">
          <a
            className="page-link"
            onClick={() => dispatch(setCurrent(prop.total))}
          >
            End
          </a>
        </li>
      </ul>
    </nav>
  );
};
export default Pagination;
