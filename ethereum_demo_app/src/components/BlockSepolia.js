import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setBlock, setBlockDetail } from '../redux/fetchBlockReducer';
import { fetchBlockDetail, fetchNewestBlock } from '../services/etherService';
import { useNavigate } from 'react-router-dom';
const BlockSepolia = ({ prop }) => {
  const navigate = useNavigate();
  const [option, setOption] = useState(0);
  const [numFind, setNumFind] = useState();
  const dispatch = useDispatch();
  const { numBlock, detail } = useSelector((state) => state.fetchBlock);
  useEffect(() => {
    if (prop === null) {
      fetchNewestBlock()
        .then(({ data }) => {
          dispatch(setBlock(data.result));
          return data.result;
        })
        .then((num) => {
          fetchBlockDetail(num).then(({ data }) => {
            console.log(data);
            dispatch(setBlockDetail(data.result));
          });
        });
    } else {
      dispatch(setBlock(prop));
      fetchBlockDetail(prop).then(({ data }) => {
        dispatch(setBlockDetail(data.result));
      });
    }
  }, []);
  return (
    <div className="card height-card width-card">
      <div className="card-header row center-ver">
        <div className="row padding-row height-card-header center-ver">
          <div className="col-4">
            <select
              onChange={(e) => {
                setOption(e.target.value);
              }}
              className="form-select"
              aria-label="Default select example"
            >
              <option value="0" selected>
                Lets find a block
              </option>
              <option value="1">Recent block</option>
              <option value="2">Find by number</option>
            </select>
          </div>
          <div className="col-4">
            <div className="row center-ver center-horz">
              {option === '2' ? (
                <input
                  onChange={(e) => setNumFind(e.target.value)}
                  className=" input-find"
                  placeholder="Find any block by number"
                ></input>
              ) : (
                numBlock
              )}
            </div>
          </div>
          <div className="col-4">
            <div className="row center-ver flex-end">
              <button
                className="bt-find"
                onClick={() => {
                  if (option === '0') {
                    return;
                  } else if (option === '1') {
                    navigate('/sepolia');
                  } else navigate('/sepolia?block=' + numFind);
                }}
              >
                Find
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="card-body row center-ver">
        <div className="row height-ele">
          <div className="col-4 ad-height">Block height</div>
          <div className="col-8">
            <div className="">{numBlock}</div>
          </div>
        </div>
        <div className="row height-ele">
          <div className="col-4 ad-height">Prev</div>
          <div className="col-8 ad-height">
            <div className="fix-content ad-height">{detail.parentHash}</div>
          </div>
        </div>
        <div className="row height-ele">
          <div className="col-4 ad-height">Hash</div>
          <div className="col-8 ad-height">
            <div className="fix-content ad-height">{detail.hash}</div>
          </div>
        </div>
        <div className="row height-ele">
          <div className="col-4 ad-height">Nonce</div>
          <div className="col-8 ad-height">
            <div className="fix-content ad-height">{detail.nonce}</div>
          </div>
        </div>
        <div className="row height-ele">
          <div className="col-4 ad-height">Number of Tx</div>
          <div className="col-8 ad-height">
            <a href={'sepolia/transaction?block=' + numBlock}>
              {detail.transactions ? detail.transactions.length : 0}
            </a>
          </div>
        </div>
        <div className="row height-ele">
          <div className="col-4 ad-height">Miner</div>
          <div className="col-8 ad-height">
            <div className="fix-content ad-height">{detail.miner}</div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default BlockSepolia;
