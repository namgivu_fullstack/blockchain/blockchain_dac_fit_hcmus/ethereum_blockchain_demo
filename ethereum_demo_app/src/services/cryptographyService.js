import { keccak256, bufferToHex, sha256, ecsign } from 'ethereumjs-util';
import { ec } from 'elliptic';
import { MerkleTree } from 'merkletreejs';

export const instance = new ec('secp256k1');
export const hashEther = (data) => {
  const hash = keccak256(Buffer.from(data));
  return hash.toString('hex');
};

export const hashBtc = (data) => {
  const hash = sha256(Buffer.from(data));
  return hash.toString('hex');
};
export const hashBlockChain = (data) => {
  const hash = sha256(Buffer.from(data));
  return hash.toString('hex');
};

export const mine = (message, difficulty) => {
  let nonce = 0;
  let targetPrefix = '0'.repeat(difficulty / 4); // Target hash prefix based on difficulty
  let foundHash = '';

  while (true) {
    const data = message + nonce;

    // Calculate the keccak256 hash
    const hash = hashEther(data);
    // Check if the hash meets the target criteria
    if (hash.startsWith(targetPrefix)) {
      foundHash = hash;
      break; // Stop mining when a suitable hash is found
    }

    nonce++; // Increment the nonce for the next iteration
  }

  return { nonce: nonce, hash: foundHash }; // The nonce that produces the valid hash
};

export const buildMerkle = async (leave) => {
  const initHash = async () => {
    let temp = await leave.map((x) => hashBtc(x));
    return temp;
  };

  let leaves = await initHash();
  const tree = new MerkleTree(leaves, hashBtc, {
    isBitcoinTree: true,
    hashLeaves: true,
    duplicateOdd: true
  });
  return tree;
};

export const ETH_tree = async (leave) => {
  const tree = new MerkleTree(leave, hashEther, {
    isBitcoinTree: false,
    hashLeaves: true,
    duplicateOdd: true
  });
  return tree;
};
export const genKeyPair = () => {
  const keyPair = instance.genKeyPair();
  const privateKeyHex = keyPair.getPrivate('hex');

  const publicKeyHex = keyPair.getPublic('hex');
  return { pub: publicKeyHex, pri: privateKeyHex };
};

export const test = () => {};
export const Sign = (hash, pri) => {
  const key = instance.keyFromPrivate(pri, 'hex');
  return key.sign(Buffer.from(hash));
};
export const Verify = (signs, original_data, pub) => {
  const key = instance.keyFromPublic(pub, 'hex');
  const signatureRBuffer = Buffer.from(signs.r, 'hex');
  const signatureSBuffer = Buffer.from(signs.s, 'hex');
  const dataBuffer = Buffer.from(hashEther(original_data));
  const signature = { r: signatureRBuffer, s: signatureSBuffer };
  return key.verify(dataBuffer, signs);
};
