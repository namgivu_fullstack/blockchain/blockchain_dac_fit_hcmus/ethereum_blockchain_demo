import axios from 'axios';
import { FETCH_URL_ETH, FETCH_URL_SEP, apiKey } from '../constant';
export const fetchNewestBlock = async () => {
  const api_string =
    FETCH_URL_SEP + `module=proxy&action=eth_blockNumber&apikey=${apiKey}`;
  return await axios.get(api_string).catch((err) => {
    throw err;
  });
};
export const fetchBlockDetail = async (numBlock) => {
  const api_string =
    FETCH_URL_SEP +
    `module=proxy&action=eth_getBlockByNumber&tag=${numBlock}&boolean=true&apikey=${apiKey}`;
  return await axios.get(api_string).catch((err) => {
    throw err;
  });
};
export const fetchTransDetail = async (hash) => {
  const api_string =
    FETCH_URL_SEP +
    `module=proxy&action=eth_getTransactionByHash&txhash=${hash}&apikey=${apiKey}`;
  return await axios.get(api_string).catch((err) => {
    throw err;
  });
};

export const getStatusTransaction = async (hash) => {
  const api_string =
    FETCH_URL_SEP +
    `module=proxy&action=eth_getTransactionReceipt&txhash=${hash}&apikey=${apiKey}`;
  return await axios.get(api_string).catch((err) => {
    throw err;
  });
};
