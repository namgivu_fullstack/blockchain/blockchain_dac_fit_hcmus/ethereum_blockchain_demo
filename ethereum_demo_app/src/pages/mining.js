import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setHash, setMessage, setNonce } from '../redux/mineReducer';
import { hashEther, mine } from '../services/cryptographyService';
import { difficulty } from '../constant';
import { useParams } from 'react-router-dom';
const Mining = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const { hash, message, nonce } = useSelector((state) => state.mine);
  const [checkNonce, setCheckNonce] = useState(false);
  useEffect(() => {
    let temp = hashEther(message);
    dispatch(setHash(temp));
  }, [message]);
  useEffect(() => {
    if (hash.substring(0, difficulty / 4) === '0'.repeat(difficulty / 4)) {
      setCheckNonce(true);
    } else {
      setCheckNonce(false);
    }
  }, [hash]);
  return (
    <div className="row padding-ver center-horz">
      <div className="card width-card">
        <div className="card-header row center-ver">
          <div className="row padding-row height-card-header center-ver">
            <div className="col">
              <ul className="nav nav-tabs">
                <li className="nav-item">
                  <a
                    className={!id ? 'nav-link active' : 'nav-link'}
                    href="/mining"
                  >
                    Mining
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    className={id === 'btc' ? 'nav-link active' : 'nav-link'}
                    href="/mining/btc"
                  >
                    Mining follow BTC
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    className={id === 'eth' ? 'nav-link active' : 'nav-link'}
                    href="/mining/eth"
                  >
                    Mining follow ETH
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="card-body row center-ver">
          <div className="row">
            <div className="mb-3">
              <label
                htmlFor="exampleFormControlTextarea1"
                className="form-label"
              >
                Your message
              </label>
              <textarea
                onChange={(e) => {
                  dispatch(setMessage(e.target.value));
                }}
                placeholder="Your message"
                className="form-control"
                id="exampleFormControlTextarea1"
                rows="3"
              ></textarea>
            </div>
            <div className="mb-3">
              <label
                htmlFor="exampleFormControlTextarea1"
                className="form-label"
              >
                Let's find Nonce
              </label>
              <input
                key={hash}
                disabled
                placeholder="Your Nonce"
                className={
                  checkNonce
                    ? 'form-control success-color'
                    : 'form-control fail-color'
                }
                id="exampleFormControlTextarea1"
                rows="3"
                value={nonce}
              ></input>
            </div>
            <div className="mb-3">
              <label
                htmlFor="exampleFormControlTextarea1"
                className="form-label"
              >
                Click me to mine
              </label>
              <button
                onClick={() => {
                  let res = mine(message, difficulty);
                  dispatch(setNonce(res.nonce));
                  dispatch(setHash(res.hash));
                }}
                className="form-control bt-cl bt-custom-control"
                // rows="3"
              >
                Mine
              </button>
            </div>
            <div className="mb-3">
              <label htmlFor="exampleFormControlInput1" className="form-label">
                Your hash
              </label>
              <input
                disabled
                className="form-control"
                id="exampleFormControlInput1"
                value={hash}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Mining;
