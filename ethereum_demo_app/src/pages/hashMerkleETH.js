import { useEffect, useState } from 'react';
import { ETH_tree } from '../services/cryptographyService';
const HashMerkleETH = () => {
  const [root_, setRoot_] = useState();

  const [trans, setTrans] = useState([
    { id: '', from: '', to: '', value: '' },
    { id: '', from: '', to: '', value: '' },
    { id: '', from: '', to: '', value: '' },
    { id: '', from: '', to: '', value: '' }
  ]);
  const toArray = (a) => {
    let res = [];
    a.forEach((data) => {
      let str =
        'from ' + data.from + ' to ' + data.to + ' value ' + data.value + 'eth';
      res.push(str);
    });
    return res;
  };

  useEffect(() => {
    let to_arr = toArray(trans);
    const buildTree = async () => {
      let res = await ETH_tree(to_arr).then((data) => {
        setRoot_(data);
      });
    };
    buildTree();
  }, [trans]);

  return (
    <div className="row padding-ver center-horz">
      <div className="card width-card">
        <div className="card-header row center-ver">
          <div className="row padding-row height-card-header center-ver">
            <div className="col">
              <ul className="nav nav-tabs">
                <li className="nav-item">
                  <a className={'nav-link'} href="/hash">
                    Hash
                  </a>
                </li>
                <li className="nav-item">
                  <a className={'nav-link'} href="/hash/merkle">
                    Merkle
                  </a>
                </li>
                <li className="nav-item">
                  <a className={'nav-link'} href="/hash/hash-merkle-btc">
                    Hash Merkle BTC
                  </a>
                </li>
                <li className="nav-item">
                  <a className={'nav-link active'} href="/hash/hash-merkle-eth">
                    Hash Merkle ETH
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="card-body row center-ver center-horz">
          <div className="row">
            <div className="mb-3">
              <div className="tree">
                <ul>
                  <li>
                    <div
                      className="node-root"
                      data-tooltip={
                        root_ ? root_.layers[2][0].toString('hex') : ''
                      }
                    >
                      Root <br />
                    </div>
                    <ul>
                      <li>
                        <div
                          className="node-root"
                          data-tooltip={
                            root_ ? root_.layers[1][0].toString('hex') : ''
                          }
                        >
                          Node-1
                        </div>
                        <ul>
                          <li>
                            <div
                              className="node-root"
                              data-tooltip={
                                root_ ? root_.layers[0][1].toString('hex') : ''
                              }
                            >
                              tx-1
                            </div>
                          </li>
                          <li>
                            <div
                              className="node-root"
                              data-tooltip={
                                root_ ? root_.layers[0][2].toString('hex') : ''
                              }
                            >
                              tx-2
                            </div>
                          </li>
                        </ul>
                      </li>
                      <li>
                        <div
                          className="node-root"
                          data-tooltip={
                            root_ ? root_.layers[1][1].toString('hex') : ''
                          }
                        >
                          Node-2
                        </div>
                        <ul>
                          <li>
                            <div
                              className="node-root"
                              data-tooltip={
                                root_ ? root_.layers[0][2].toString('hex') : ''
                              }
                            >
                              tx-3
                            </div>
                          </li>
                          <li>
                            <div
                              className="node-root"
                              data-tooltip={
                                root_ ? root_.layers[0][3].toString('hex') : ''
                              }
                            >
                              tx-4
                            </div>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>

            <div className="mb-3">
              <label
                htmlFor="exampleFormControlTextarea1"
                className="form-label"
              >
                Let's test
              </label>
              <div className="card">
                <div className="card-body row">
                  {trans.map((e, ind) => {
                    return (
                      <div
                        className="row center-ver center-horz"
                        style={{ paddingBottom: '10px' }}
                        key={ind}
                      >
                        <div>Tx{ind + 1}:</div>
                        <div className="col-4">
                          <div>
                            <input
                              onChange={(e) => {
                                const updatedTrans = [...trans];
                                updatedTrans[ind] = {
                                  ...updatedTrans[ind],
                                  from: e.target.value
                                };
                                setTrans(updatedTrans);
                              }}
                              placeholder="From"
                            ></input>
                          </div>
                        </div>
                        <div className="col-4">
                          <div>
                            <input
                              onChange={(e) => {
                                const updatedTrans = [...trans];
                                updatedTrans[ind] = {
                                  ...updatedTrans[ind],
                                  to: e.target.value
                                };
                                setTrans(updatedTrans);
                              }}
                              placeholder="To"
                            ></input>
                          </div>
                        </div>
                        <div className="col-4">
                          <div>
                            <input
                              onChange={(e) => {
                                const updatedTrans = [...trans];
                                updatedTrans[ind] = {
                                  ...updatedTrans[ind],
                                  value: e.target.value
                                };
                                setTrans(updatedTrans);
                              }}
                              type="number"
                              placeholder="Value"
                            ></input>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
            <div className="mb-3">
              <label htmlFor="exampleFormControlInput1" className="form-label">
                Your hash
              </label>
              <input
                disabled
                className="form-control"
                id="exampleFormControlInput1"
                value={root_ ? root_.layers[2][0].toString('hex') : ''}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default HashMerkleETH;
