import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setHash, setMessage } from '../redux/hashReducer';
import hashimg from '../assets/hash.png';
import { buildMerkle, hashEther } from '../services/cryptographyService';
const HashPage = () => {
  const dispatch = useDispatch();
  const { hash, message } = useSelector((state) => state.hash);

  useEffect(() => {
    let temp = hashEther(message);
    dispatch(setHash(temp));
  }, [message]);

  return (
    <div className="row padding-ver center-horz">
      <div className="card width-card">
        <div className="card-header row center-ver">
          <div className="row padding-row height-card-header center-ver">
            <div className="col">
              <ul className="nav nav-tabs">
                <li className="nav-item">
                  <a className={'nav-link active'} href="/hash">
                    Hash
                  </a>
                </li>
                <li className="nav-item">
                  <a className={'nav-link'} href="/hash/merkle">
                    Merkle
                  </a>
                </li>
                <li className="nav-item">
                  <a className={'nav-link'} href="/hash/hash-merkle-btc">
                    Hash Merkle BTC
                  </a>
                </li>
                <li className="nav-item">
                  <a className={'nav-link'} href="/hash/hash-merkle-eth">
                    Hash Merkle ETH
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="card-body row center-ver center-horz">
          <div className="row">
            <h5 class="card-title">What is the concept named "Hash"?</h5>
            <div className="mb-3">
              <div className="custom-text">
                <p class="card-text">
                  Imagine you have a magical machine that can turn any item into
                  a unique, fixed-size code. This code is like a fingerprint for
                  that item – no two items produce the same code, and even a
                  tiny change in the item results in a completely different
                  code. This magical transformation is what we call a "hash."
                </p>
              </div>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginBottom: '20px'
                }}
              >
                <img
                  src={hashimg}
                  style={{ width: '500px', height: '300px' }}
                ></img>
              </div>
              <div className="custom-text">
                <p class="card-text">
                  In the realm of blockchain technology, hash functions stand as
                  the cryptographic workhorses that underpin the security,
                  integrity, and efficiency of the entire ecosystem. These
                  seemingly magical algorithms serve as the invisible guardians
                  of data, ensuring that transactions remain tamper-proof and
                  that the intricate architecture of the blockchain remains
                  robust and unassailable.
                </p>
              </div>
              <div className="custom-text">
                <p class="card-text">
                  At the heart of their application lies the principle of
                  hashing, a process that takes input data and transforms it
                  into a fixed-size, seemingly random string of characters. This
                  process is not only deterministic, meaning the same input
                  always produces the same output, but it's also designed in a
                  way that even the smallest change in the input results in a
                  vastly different hash. This unique property forms the bedrock
                  of blockchain's integrity, as even the slightest tampering
                  with data reverberates through the entire chain, immediately
                  alerting the network to any malfeasance.
                </p>
              </div>
              <div className="custom-text">
                <p class="card-text">
                  At the core of Bitcoin's architecture lies the renowned
                  SHA-256 (Secure Hash Algorithm 256-bit). Developed by the
                  National Security Agency (NSA), this algorithm transforms
                  input data into a fixed 256-bit hash value.
                </p>
              </div>
              <div className="custom-text">
                <p class="card-text">
                  <span>While Ethereum</span>, taking a distinct path in the
                  blockchain landscape. Derived from the Keccak family of
                  cryptographic sponge functions, this algorithm also generates
                  a 256-bit hash output. Ethereum's choice reflects its
                  commitment to enabling advanced cryptographic techniques,
                  fostering the execution of smart contracts and decentralized
                  applications. Keccak-256 underpins Ethereum's smart contract
                  execution, ensuring the security and integrity of contract
                  interactions within its dynamic ecosystem.
                </p>
              </div>
              <div className="custom-text">
                <p>
                  <a
                    target="_blank"
                    href="https://www.crypto101.io/Crypto101.pdf"
                    class="card-text"
                  >
                    To more detail about hash click here
                  </a>
                  <span> or Keccak </span>
                  <a
                    target="_blank"
                    href="https://en.wikipedia.org/wiki/SHA-3"
                    class="card-text"
                  >
                    click here.
                  </a>
                </p>
              </div>
              <div className="custom-text">
                <p class="card-text">
                  Let's test keccak-256 below to feel how amazing it is.
                </p>
              </div>
            </div>

            <div className="mb-3">
              <label
                htmlFor="exampleFormControlTextarea1"
                className="form-label"
              >
                Let's hash your message
              </label>
              <textarea
                onChange={(e) => {
                  dispatch(setMessage(e.target.value));
                }}
                placeholder="Your message"
                className="form-control"
                id="exampleFormControlTextarea1"
                rows="3"
              ></textarea>
            </div>

            <div className="mb-3">
              <label htmlFor="exampleFormControlInput1" className="form-label">
                Your hash
              </label>
              <input
                disabled
                type="email"
                className="form-control"
                id="exampleFormControlInput1"
                value={hash}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default HashPage;
