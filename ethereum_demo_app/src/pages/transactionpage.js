import { useEffect } from 'react';
import BlockSepolia from '../components/BlockSepolia';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { setBlockDetail } from '../redux/fetchBlockReducer';
import { fetchBlockDetail, fetchTransDetail } from '../services/etherService';
import { setNum, setTransaction } from '../redux/fetchTransactionsReducer';
import Pagination from '../components/pagination';
import { setTotal } from '../redux/pagecontrolReducer';
const TransactionPage = () => {
  const features = ['Txn Hash', ' Block', 'From', 'To', 'Value', 'Txn Fee'];
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const block = queryParams.get('block');
  const dispatch = useDispatch();
  const { numOfTran, transactions } = useSelector((state) => state.trans);
  const { current, total } = useSelector((state) => state.page);
  const col_table = [3, 1, 3, 3, 1, 1];
  useEffect(() => {
    console.log('fetch');
    const fetchData = async () => {
      if (transactions.length === 0) {
        const blockDetail = await fetchBlockDetail(block);
        const transactionHashes = blockDetail.data.result.transactions;
        dispatch(setNum(transactionHashes.length));
        // const transactionsData = await Promise.all(
        //   transactionHashes.map(async (hash) => {
        //     const transDetail = await fetchTransDetail(hash);
        //     return transDetail.data.result;
        //   })
        // );
        dispatch(setTransaction(transactionHashes));
        dispatch(setTotal(transactionHashes.length));
      }
    };
    fetchData();
  }, []);
  return (
    <div className="row padding-ver center-horz">
      <div className="row custom-box">
        <div className="table-responsive">
          <table className="table">
            <thead>
              <tr style={{ display: 'flex' }}>
                {features.map((data, ind) => {
                  return (
                    <th
                      scope="col"
                      key={ind}
                      className={'col-' + col_table[ind]}
                    >
                      {data}
                    </th>
                  );
                })}
              </tr>
            </thead>
            <tbody>
              {transactions.length !== 0 ? (
                transactions
                  .slice()
                  .filter((e, ind) => {
                    if (current * 10 + 10 > numOfTran)
                      return ind < numOfTran && ind >= numOfTran - 10;
                    else return ind >= current * 10 && ind < current * 10 + 10;
                  })
                  .map((data, ind) => {
                    return (
                      <tr key={ind} style={{ display: 'flex' }}>
                        <th scope="row" className="col-3">
                          <div>
                            <span className="fix-content">
                              <a href={'/tx/' + data.hash}>{data.hash}</a>
                            </span>
                          </div>
                        </th>
                        <td className="col-1">
                          <div>
                            <span className="fix-content">
                              <a href={'/sepolia?block=' + data.blockNumber}>
                                {parseInt(data.blockNumber, 16)}
                              </a>
                            </span>
                          </div>
                        </td>
                        <td className="col-3">
                          <div>
                            <span className="fix-content">
                              <a href={'/sepolia/address/' + data.from}>
                                {data.from}
                              </a>
                            </span>
                          </div>
                        </td>
                        <td className="col-3">
                          <div>
                            <span className="fix-content">
                              <a href={'/sepolia/address/' + data.to}>
                                {data.to}
                              </a>
                            </span>
                          </div>
                        </td>
                        <td className="col-1">
                          <div>
                            <span className="fix-content">
                              {data.value / Math.pow(10, 18)}
                            </span>
                          </div>
                        </td>
                        <td className="col-1">
                          <div>
                            <span className="fix-content">
                              {(
                                parseInt(data.gas * data.gasPrice) /
                                Math.pow(10, 18)
                              ).toFixed(8)}
                            </span>
                          </div>
                        </td>
                      </tr>
                    );
                  })
              ) : (
                <div
                  className="center-ver center-horz"
                  style={{ display: 'flex', height: '300px' }}
                >
                  <h3>No data found</h3>
                </div>
              )}
            </tbody>
          </table>
        </div>
        <div className="custom-pagination">
          {total ? (
            <Pagination
              key={current}
              prop={{
                total: Math.ceil(total / 10),
                current: current
              }}
            />
          ) : (
            <></>
          )}
        </div>
      </div>
    </div>
  );
};
export default TransactionPage;
