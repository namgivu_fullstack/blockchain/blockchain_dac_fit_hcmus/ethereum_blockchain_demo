import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setHash, setMessage } from '../redux/hashReducer';
import hashimg from '../assets/hash.png';
import { buildMerkle, hashEther } from '../services/cryptographyService';
const Merkle = () => {
  const dispatch = useDispatch();
  const { hash, message } = useSelector((state) => state.hash);

  useEffect(() => {
    let temp = hashEther(message);
    dispatch(setHash(temp));
  }, [message]);

  return (
    <div className="row padding-ver center-horz">
      <div className="card width-card">
        <div className="card-header row center-ver">
          <div className="row padding-row height-card-header center-ver">
            <div className="col">
              <ul className="nav nav-tabs">
                <li className="nav-item">
                  <a className={'nav-link'} href="/hash">
                    Hash
                  </a>
                </li>
                <li className="nav-item">
                  <a className={'nav-link active'} href="/merkle">
                    Merkle
                  </a>
                </li>
                <li className="nav-item">
                  <a className={'nav-link'} href="/hash/hash-merkle-btc">
                    Hash Merkle BTC
                  </a>
                </li>
                <li className="nav-item">
                  <a className={'nav-link'} href="/hash/hash-merkle-eth">
                    Hash Merkle ETH
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="card-body row center-ver center-horz">
          <div className="row">
            <h5 class="card-title">What is "Merkle"?</h5>
            <div className="mb-3">
              <div className="custom-text">
                <p class="card-text">
                  Merkle trees, named after their creator Ralph Merkle, are
                  cryptographic structures that compactly verify data integrity.
                  Utilized in blockchain technology and digital signatures,
                  these hierarchical trees efficiently validate large datasets
                  by hashing them into compact forms, ensuring security and
                  tamper detection. By linking blocks in blockchains and
                  authenticating documents, Merkle trees play a pivotal role in
                  data security, verification, and the trust underpinning modern
                  digital systems.
                </p>
              </div>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginBottom: '20px'
                }}
              >
                <img
                  src="https://user-images.githubusercontent.com/168240/43616375-15330c32-9671-11e8-9057-6e61c312c856.png"
                  style={{ width: '400px', height: '200px' }}
                ></img>
              </div>
              <div className="custom-text">
                <p class="card-text">
                  The structure resembles an inverted tree, where each leaf node
                  represents a piece of data (often referred to as a "hash"),
                  and every non-leaf node is a hash of its child nodes. This
                  process continues up the tree until a single root hash is
                  generated, known as the "Merkle Root."
                </p>
              </div>
              <div className="custom-text">
                <p class="card-text">
                  Let's take an example to grasp why blockchains harness the
                  efficiency of Merkle Trees for data verification and to
                  minimize the size of the chain
                </p>
              </div>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginBottom: '20px'
                }}
              >
                <img
                  src="https://user-images.githubusercontent.com/168240/204968384-dbd16f5b-415c-4cc6-b993-5bbd7599ec8b.png"
                  style={{ width: '400px', height: '250px' }}
                ></img>
              </div>
              <div className="custom-text">
                <p class="card-text">
                  Merkle Trees are a natural fit for blockchain architecture.
                  Taking the tree example into account: when verifying node H3,
                  it suffices to have nodes H5 and H6 for generating the Merkle
                  root, eliminating the necessity of computing all nodes in the
                  process.
                </p>
              </div>
              <div className="custom-text">
                <p class="card-text">Taking a look to Bitcoin's merkle tree</p>
              </div>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginBottom: '20px'
                }}
              >
                <img
                  src="https://user-images.githubusercontent.com/168240/43616417-46d3293e-9671-11e8-81c3-8cdf7f8ddd77.png"
                  style={{ width: '400px', height: '200px' }}
                ></img>
              </div>
              <div className="custom-text">
                <p class="card-text">
                  In Bitcoin, Merkle tree have a bit of difference. Bitcoin's
                  utilization of double hashing serves to bolster the security
                  and integrity of its blockchain. By employing two rounds of
                  hashing, Bitcoin enhances resistance against accidental hash
                  collisions and strengthens protection against second preimage
                  attacks. This double hashing approach amplifies the avalanche
                  effect, ensuring even the slightest input alteration results
                  in a dramatically different hash output. This practice has
                  become a standard in Bitcoin's proof-of-work system,
                  contributing to network consistency and the maintenance of
                  cryptographic properties.
                </p>
              </div>
              <div className="custom-text">
                <p>
                  <a
                    target="_blank"
                    href="https://github.com/merkletreejs/merkletreejs#example"
                    class="card-text"
                  >
                    To find more resources click here.
                  </a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Merkle;
