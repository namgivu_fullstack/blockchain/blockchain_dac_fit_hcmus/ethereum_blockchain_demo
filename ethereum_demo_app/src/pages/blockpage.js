import { useEffect } from 'react';
import BlockSepolia from '../components/BlockSepolia';
import { useLocation } from 'react-router-dom';
const BlockPage = () => {
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const block = queryParams.get('block');
  return (
    <div className="row padding-ver center-horz">
      <BlockSepolia prop={block} key={block} />
    </div>
  );
};
export default BlockPage;
