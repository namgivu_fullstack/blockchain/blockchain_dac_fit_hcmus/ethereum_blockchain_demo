import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getStatusTransaction } from '../services/etherService';
const TransactionDetail = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const [transaction, setTransaction] = useState();
  useEffect(() => {
    console.log(id);
    const fetchTransaction = async () => {
      const res = await getStatusTransaction(id);
      setTransaction(res.data.result);
    };
    fetchTransaction();
  }, []);
  return (
    <div className="row padding-ver center-horz">
      <div className="row card  width-card">
        <div className="card-body">
          <div className="row padding-row">
            <div className="col-4">Transaction Hash</div>
            <div className="col-8">
              {transaction ? transaction.transactionHash : null}
            </div>
          </div>
          <div className="row padding-row">
            <div className="col-4">Status</div>
            <div className="col-8">
              {transaction
                ? transaction.status === 1
                  ? 'Success'
                  : 'Fail'
                : null}
            </div>
          </div>
          <div className="row padding-row">
            <div className="col-4">Block</div>
            <div className="col-8">
              {transaction ? parseInt(transaction.blockNumber, 16) : null}
            </div>
          </div>
          {/* <div className="row padding-row">
            <div className="col-4">Timestamp</div>
            <div className="col-8"></div>
          </div> */}
          <div className="row padding-row">
            <div className="col-4">From</div>
            <div className="col-8">{transaction ? transaction.from : null}</div>
          </div>
          <div className="row padding-row">
            <div className="col-4">To</div>
            <div className="col-8">{transaction ? transaction.to : null}</div>
          </div>
          {/* <div className="row padding-row">
            <div className="col-4">Value</div>
            <div className="col-8"></div>
          </div> */}
          <div className="row padding-row">
            <div className="col-4">Transaction fee</div>
            <div className="col-8">
              {transaction
                ? (
                    parseInt(
                      transaction.effectiveGasPrice * transaction.gasUsed
                    ) / Math.pow(10, 18)
                  ).toFixed(8)
                : null}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default TransactionDetail;
