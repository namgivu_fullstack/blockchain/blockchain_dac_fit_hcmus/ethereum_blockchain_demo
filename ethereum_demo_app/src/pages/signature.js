import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import { Sign, genKeyPair, hashEther } from '../services/cryptographyService';
import { setPrivateKey, setPublicKey } from '../redux/sigReducer';
const Signature = () => {
  const dispatch = useDispatch();
  const { pub, pri } = useSelector((state) => state.sign);
  const [submit, setSubmit] = useState(false);
  const [mes, setMes] = useState('');
  const [sign, setSign] = useState(null);

  useEffect(() => {
    const res = genKeyPair();
    dispatch(setPrivateKey(res.pri));
    dispatch(setPublicKey(res.pub));
  }, [submit]);

  useEffect(() => {
    if (mes === '') {
      setSign('');
    } else if (mes !== '' && pri !== null) {
      let hash = hashEther(mes);
      let signature = Sign(hash, pri);
      setSign(signature);
    }
  }, [mes]);
  return (
    <div className="row padding-ver center-horz">
      <div className="card width-card">
        <div className="card-header row center-ver">
          <div className="row padding-row height-card-header center-ver">
            <div className="col">
              <ul className="nav nav-tabs">
                <li className="nav-item">
                  <a className={'nav-link active'} href="/signature">
                    Sign
                  </a>
                </li>
                <li className="nav-item">
                  <a className={'nav-link'} href="/signature/verify">
                    Verify
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="card-body row center-ver center-horz">
          <div className="row">
            <h5 class="card-title">
              What is the concept named "Signature and Veriy"?
            </h5>
            <div className="mb-3">
              <div className="custom-text">
                <p class="card-text">
                  The concept of "Signature and Verify" pertains to a
                  fundamental cryptographic technique used to ensure the
                  authenticity and integrity of digital information. It involves
                  two key operations
                </p>
              </div>
              <div className="custom-text">
                <p class="card-text">
                  Signature (Signing): In this operation, a digital signature is
                  generated using a private key. The private key is owned by the
                  signer and is kept secret. The process involves applying a
                  cryptographic algorithm to the data being signed, resulting in
                  a unique digital signature. This signature is appended to the
                  data, creating a signed message. The signature provides proof
                  that the data has been signed by the owner of the private key
                  and has not been altered since signing.
                </p>
              </div>
              <div className="custom-text">
                <p class="card-text">
                  Verification (Verifying): The verification process involves
                  using a corresponding public key, which is associated with the
                  private key used for signing. The recipient of the signed
                  message, or anyone else, can use the public key to verify the
                  authenticity and integrity of the message. The signature is
                  decrypted or processed using the public key, and if the
                  decrypted signature matches the original data, it confirms
                  that the message has not been tampered with and was indeed
                  signed by the private key holder.{' '}
                </p>
              </div>
              <div className="custom-text">
                <p class="card-text">
                  The key pair intended for internet use must be safeguarded
                  against hackers. Consequently, the creation of this key pair
                  requires a robust algorithm to ensure security.
                </p>
              </div>
              <div className="custom-text">
                <p class="card-text">
                  Numerous algorithms exist for generating key pairs, yet this
                  page exclusively delves into ECC, renowned for being an
                  exceptionally robust key pair generator.
                </p>
              </div>
              <div className="custom-text">
                <p class="card-text">
                  ECC (Elliptic Curve Cryptography) is a modern and efficient
                  cryptographic technique used for creating key pairs. It offers
                  strong security with shorter key lengths compared to
                  traditional methods like RSA. ECC operates on the mathematical
                  properties of elliptic curves, providing enhanced security and
                  efficiency for various applications such as digital signatures
                  and encryption. Its compactness and resistance to quantum
                  attacks make ECC a popular choice in modern cryptography.
                </p>
              </div>
              <div className="custom-text">
                <p class="card-text">
                  In ECC, the "r" and "s" components are crucial elements of a
                  digital signature. "r" represents a point on the elliptic
                  curve derived from the private key and message, while "s" is
                  computed based on the private key, message, and "r" component.
                  Together, these components form a secure digital signature
                  that verifies the signer's identity and message integrity
                  during verification. That's why the form below have 2
                  signature r and s after you sign your message.
                </p>
              </div>

              <div className="custom-text">
                <p>
                  <a
                    target="_blank"
                    href="https://en.wikipedia.org/wiki/Public-key_cryptography"
                    class="card-text"
                  >
                    To more detail about key-pair click here
                  </a>
                  <span> or ECC </span>
                  <a
                    target="_blank"
                    href="https://en.wikipedia.org/wiki/Elliptic-curve_cryptography"
                    class="card-text"
                  >
                    click here.
                  </a>
                </p>
              </div>
              <div className="custom-text">
                <p class="card-text">
                  You can create the key-pair as well as signing to your message
                  below.
                </p>
              </div>
              <div className="custom-text"></div>
            </div>

            <div className="mb-3">
              <label
                htmlFor="exampleFormControlTextarea1"
                className="form-label"
              >
                Generate key pair
              </label>
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  alignItems: 'center'
                }}
              >
                <div className="col-4">
                  <button
                    onClick={() => {
                      setSubmit((prev) => !prev);
                    }}
                    className="bt-find"
                    style={{
                      height: '40px'
                    }}
                  >
                    Genkey
                  </button>
                </div>
              </div>
            </div>
            <div className="mb-3">
              <label
                htmlFor="exampleFormControlTextarea1"
                className="form-label"
              >
                Private Key
              </label>
              <input
                style={{ fontSize: '14px' }}
                disabled
                value={pri ? pri : ''}
                placeholder="Your message"
                className="form-control"
                id="exampleFormControlTextarea1"
                rows="3"
              ></input>
            </div>
            <div className="mb-3">
              <label
                htmlFor="exampleFormControlTextarea1"
                className="form-label"
              >
                Public Key
              </label>
              <input
                style={{ fontSize: '14px' }}
                value={pub ? pub : ''}
                placeholder="Your message"
                className="form-control"
                id="exampleFormControlTextarea1"
                rows="3"
                disabled
              ></input>
            </div>
            <div className="mb-3">
              <label
                htmlFor="exampleFormControlTextarea1"
                className="form-label"
              >
                Your message
              </label>
              <textarea
                onChange={(e) => {
                  setMes(e.target.value);
                }}
                placeholder="Your message"
                className="form-control"
                id="exampleFormControlTextarea1"
                rows="3"
              ></textarea>
            </div>
            <div className="mb-3">
              <label htmlFor="exampleFormControlInput1" className="form-label">
                Your R signature
              </label>
              <input
                disabled
                className="form-control"
                id="exampleFormControlInput1"
                value={sign ? sign.r.toString('hex') : ''}
              />
            </div>
            <div className="mb-3">
              <label htmlFor="exampleFormControlInput1" className="form-label">
                Your S signature
              </label>
              <input
                disabled
                className="form-control"
                id="exampleFormControlInput1"
                value={sign ? sign.s.toString('hex') : ''}
              />
            </div>
            <div className="mb-3">
              {sign ? (
                <button
                  onClick={() => {
                    localStorage.setItem('pri', pri);
                    localStorage.setItem('pub', pub);
                    localStorage.setItem('r', sign.r.toString('hex'));
                    localStorage.setItem('s', sign.s.toString('hex'));
                  }}
                  style={{ width: '150px', height: '40px' }}
                  className="bt-find"
                >
                  Click to save all
                </button>
              ) : (
                <button
                  disabled
                  style={{
                    width: '150px',
                    height: '40px',
                    backgroundColor: 'rgb(137, 137, 246)',
                    color: '#ddd'
                  }}
                >
                  Click to save all
                </button>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Signature;
