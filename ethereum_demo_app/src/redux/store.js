// store/store.js
import fetchBlockReducer from './fetchBlockReducer';
import { combineReducers, configureStore } from '@reduxjs/toolkit';
import hashReducer from './hashReducer';
import fetchTransactionsReducer from './fetchTransactionsReducer';
import pagecontrolReducer from './pagecontrolReducer';
import mineReducer from './mineReducer';
import sigReducer from './sigReducer';
const rootReducer = combineReducers({
  fetchBlock: fetchBlockReducer,
  hash: hashReducer,
  trans: fetchTransactionsReducer,
  page: pagecontrolReducer,
  mine: mineReducer,
  sign: sigReducer
});
const store = configureStore({
  reducer: rootReducer
});

export default store;
