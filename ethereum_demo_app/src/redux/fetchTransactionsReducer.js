// store/reducers/counterReducer.js
import { createSlice } from '@reduxjs/toolkit';
const transSlice = createSlice({
  name: 'trans',
  initialState: { numOfTran: 0, transactions: [] },
  reducers: {
    setNum: (state, action) => {
      state.numOfTran = action.payload;
    },
    setTransaction: (state, action) => {
      state.transactions = action.payload;
    },
    refresh: (state) => {
      state.transactions = [];
      state.numOfTran = 0;
    }
  }
});

export const { setNum, setTransaction, refresh } = transSlice.actions;
export default transSlice.reducer;
