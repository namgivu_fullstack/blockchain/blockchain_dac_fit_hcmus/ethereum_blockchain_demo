// store/reducers/counterReducer.js
import { createSlice } from '@reduxjs/toolkit';
const hashSlice = createSlice({
  name: 'hash',
  initialState: { hash: '', message: '' },
  reducers: {
    setHash: (state, action) => {
      state.hash = action.payload;
    },
    setMessage: (state, action) => {
      state.message = action.payload;
    },
    refresh: (state) => {
      state.hash = null;
      state.message = '';
    }
  }
});

export const { setHash, setMessage, refresh } = hashSlice.actions;
export default hashSlice.reducer;
