// store/reducers/counterReducer.js
import { createSlice } from '@reduxjs/toolkit';
const mineHash = createSlice({
  name: 'mine',
  initialState: { hash: '', message: '', nonce: 0 },
  reducers: {
    setHash: (state, action) => {
      state.hash = action.payload;
    },
    setMessage: (state, action) => {
      state.message = action.payload;
    },
    setNonce: (state, action) => {
      state.nonce = action.payload;
    },
    refresh: (state) => {
      state.hash = null;
      state.message = '';
      state.nonce = 0;
    }
  }
});

export const { setHash, setMessage, refresh, setNonce } = mineHash.actions;
export default mineHash.reducer;
