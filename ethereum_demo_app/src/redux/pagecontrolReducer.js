// store/reducers/counterReducer.js
import { createSlice } from '@reduxjs/toolkit';
const pageSlice = createSlice({
  name: 'page',
  initialState: { current: 0, limit: 10, total: null },
  reducers: {
    setLimit: (state, action) => {
      state.limit = action.payload;
    },
    setCurrent: (state, action) => {
      state.current = action.payload;
    },
    setTotal: (state, action) => {
      state.total = action.payload;
    },
    refresh: (state) => {
      state.limit = 0;
      state.current = 0;
      state.total = 0;
    }
  }
});

export const { setCurrent, setLimit, refresh, setTotal } = pageSlice.actions;
export default pageSlice.reducer;
