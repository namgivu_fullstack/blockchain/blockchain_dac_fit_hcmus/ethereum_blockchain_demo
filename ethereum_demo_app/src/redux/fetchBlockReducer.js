// store/reducers/counterReducer.js
import { createSlice } from '@reduxjs/toolkit';
const fetchBlockSlice = createSlice({
  name: 'fetchBlock',
  initialState: { numBlock: null, detail: {} },
  reducers: {
    setBlock: (state, action) => {
      state.numBlock = action.payload;
    },
    setBlockDetail: (state, action) => {
      state.detail = action.payload;
    },
    refresh: (state) => {
      state.detail = {};
      state.numBlock = null;
    }
  }
});

export const { setBlock, setBlockDetail, refresh } = fetchBlockSlice.actions;
export default fetchBlockSlice.reducer;
