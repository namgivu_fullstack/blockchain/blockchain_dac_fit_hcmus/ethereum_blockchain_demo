// store/reducers/counterReducer.js
import { createSlice } from '@reduxjs/toolkit';
const signSlice = createSlice({
  name: 'hash',
  initialState: { pub: null, pri: null },
  reducers: {
    setPublicKey: (state, action) => {
      state.pub = action.payload;
    },
    setPrivateKey: (state, action) => {
      state.pri = action.payload;
    },

    refresh: (state) => {
      state.pub = '';
      state.pri = '';
      state.sign = '';
    }
  }
});

export const { setPrivateKey, setPublicKey } = signSlice.actions;
export default signSlice.reducer;
