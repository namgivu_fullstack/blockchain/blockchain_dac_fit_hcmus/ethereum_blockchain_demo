import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/index.css';
import MainPage from './pages/mainpage';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import BlockPage from './pages/blockpage';
import HashPage from './pages/hashpage';
import TransactionDetail from './pages/transactiondetailpage';
import TransactionPage from './pages/transactionpage';
import Nav from './components/nav';
import Mining from './pages/mining';
import { Buffer } from 'buffer';
import Signature from './pages/signature';
import HashMerkleBTC from './pages/hashMerkleBTC';
import HashMerkleETH from './pages/hashMerkleETH';
import VerifyPage from './pages/verify';
import Merkle from './pages/merkle';
import { useEffect } from 'react';
window.Buffer = Buffer; // Add this line to make Buffer available globally

function App() {
  useEffect(() => {
    localStorage.clear();
  }, []);
  return (
    <div className="container-fruid ad-height">
      <div className="row fix-nav ">
        <Nav />
      </div>
      <div className="container">
        <Router>
          <Routes>
            <Route exact path="/" element={<MainPage />} />
            <Route path="/sepolia" element={<BlockPage />} />
            <Route path="/hash" element={<HashPage />} />
            <Route path="/hash/merkle" element={<Merkle />} />
            <Route path="/hash/hash-merkle-btc" element={<HashMerkleBTC />} />
            <Route path="/hash/hash-merkle-eth" element={<HashMerkleETH />} />
            <Route path="/sepolia/transaction" element={<TransactionPage />} />
            <Route path="/tx/:id" element={<TransactionDetail />} />
            <Route path="/mining" element={<Mining />} />
            <Route path="/mining/:id" element={<Mining />} />
            <Route path="/signature" element={<Signature />} />
            <Route path="/signature/verify" element={<VerifyPage />} />
          </Routes>
        </Router>
      </div>
    </div>
  );
}

export default App;
